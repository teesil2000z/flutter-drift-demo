import 'package:flutter_drift_demo/database/database.dart';
import 'package:flutter_drift_demo/database/platform/platform.dart';
import 'package:get_it/get_it.dart';

final di = GetIt.instance;

class DependencyInjectionManager {
  static Future<void> initDependencies() async {
    registerDatabase();
  }

  static void registerDatabase() {
    di.registerLazySingleton(() => MyDatabase(DBCreator.createDatabaseConnection('dbname')));
  }
}