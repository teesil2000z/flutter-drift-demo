import 'package:bloc/bloc.dart';
import 'package:drift/drift.dart';
import 'package:flutter_drift_demo/database/database.dart';

part 'todo_edit_dialog_state.dart';

class TodoEditDialogCubit extends Cubit<TodoEditDialogState>{
  TodoEditDialogCubit(TodoEditDialogState initialState) : super(initialState);
  
  void updateDescription(String description){
    emit(state.copyWith(
      description: description
    ));
  }

  void updateSelectedDate(DateTime selectedDate){
    emit(state.copyWith(
      selectedDate: selectedDate
    ));
  }

  void save(MyDatabase db){ 
    db.allTodoEntries.updateTodoEntry(TodoEntriesCompanion(
      id: Value(state.entry.id),
      category: Value(state.entry.category),
      dueDate: Value(state.selectedDate),
      description: Value(state.description)
    ));
  }
}