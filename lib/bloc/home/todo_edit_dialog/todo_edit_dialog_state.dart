part of 'todo_edit_dialog_bloc.dart';

class TodoEditDialogState{
  final TodoEntry entry;
  final String description;
  final DateTime selectedDate;

  const TodoEditDialogState({
    required this.description,
    required this.entry,
    required this.selectedDate
  });

  TodoEditDialogState copyWith({
    TodoEntry? entry,
    String? description,
    DateTime? selectedDate
  }){
    return TodoEditDialogState(
      description: description ?? this.description, 
      entry: entry ?? this.entry,
      selectedDate: selectedDate ?? this.selectedDate
    );
  }
}