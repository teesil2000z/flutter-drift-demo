import 'package:drift/drift.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_drift_demo/bloc/home/home_state.dart';
import 'package:flutter_drift_demo/database/database.dart';

class HomeCubit extends Cubit<HomeState>{
  HomeCubit(HomeState initialState) : super(initialState);

  void updateTodoDescription(String todoDescription){
    emit(state.copyWith(todoDescription: todoDescription));
  }

  void save(MyDatabase db){
    db.allTodoEntries.createTodoEntry(TodoEntriesCompanion.insert(
      description: state.todoDescription,
      dueDate: Value(DateTime.now()),
      category: const Value(null),
    ));
  }
}