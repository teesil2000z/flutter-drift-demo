import 'package:flutter_drift_demo/model/todo_entry_with_category.dart';

class HomeState{
  final Stream<List<TodoEntryWithCategory>> entries;
  final String todoDescription; 

  const HomeState({
    required this.entries,
    required this.todoDescription
  });

  HomeState copyWith({
    Stream<List<TodoEntryWithCategory>>? entries,
    String? todoDescription
  }){
    return HomeState(
      entries: entries ?? this.entries, 
      todoDescription: todoDescription ?? this.todoDescription
    );
  }
}