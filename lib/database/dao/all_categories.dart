import 'package:flutter_drift_demo/database/database.dart';
import 'package:flutter_drift_demo/database/definition/todo_entry.dart';
import 'package:flutter_drift_demo/database/definition/category.dart';
import 'package:drift/drift.dart';
import 'package:flutter_drift_demo/model/category_with_count.dart';

part 'all_categories.g.dart';

@DriftAccessor(tables:[TodoEntries, Categories])
class AllCategories extends DatabaseAccessor<MyDatabase> with _$AllCategoriesMixin {
  AllCategories(MyDatabase attachedDatabase) : super(attachedDatabase);
  
  Future<void> deleteCategory(Category category) {
    return transaction(() async {
      // First, move todo entries that might remain into the default category
      await (todoEntries.update()
            ..where((todo) => todo.category.equals(category.id)))
          .write(const TodoEntriesCompanion(category: Value(null)));

      // Then, delete the category
      await categories.deleteOne(category);
    });
  }
  Stream<List<CategoryWithCount>> categoriesWithCount() {
    return select(categories).map((row) {
      final category = Category(id: row.id, name: row.name, color: row.color);

      return CategoryWithCount(category, row.id);
    }).watch();
  }
}