import 'package:flutter_drift_demo/database/database.dart';
import 'package:flutter_drift_demo/database/definition/todo_entry.dart';
import 'package:flutter_drift_demo/database/definition/category.dart';
import 'package:drift/drift.dart';
import 'package:flutter_drift_demo/model/todo_entry_with_category.dart';

part 'all_todo_entries.g.dart';

@DriftAccessor(tables:[TodoEntries, Categories])
class AllTodoEntries extends DatabaseAccessor<MyDatabase> with _$AllTodoEntriesMixin {
  AllTodoEntries(MyDatabase attachedDatabase) : super(attachedDatabase);

  Stream<List<TodoEntryWithCategory>> entriesInCategory(int? categoryId) {
    final query = select(todoEntries).join([
      leftOuterJoin(categories, categories.id.equalsExp(todoEntries.category))
    ]);

    if (categoryId != null) {
      query.where(categories.id.equals(categoryId));
    } else {
      query.where(categories.id.isNull());
    }

    return query.map((row) {
      return TodoEntryWithCategory(
        entry: row.readTable(todoEntries),
        category: row.readTableOrNull(categories),
      );
    }).watch();
  }

  Future<int> updateTodoEntry(TodoEntriesCompanion companion) {
    return (update(todoEntries)
          ..where((tbl) => tbl.id.equals(companion.id.value)))
        .write(companion);
  }

  Future<int> createTodoEntry(TodoEntriesCompanion companion) {
    return into(todoEntries).insert(companion);
  }

  Future<int> deleteTodoEntry(int entryId) async {
    return transaction(() async {
      return await (delete(todoEntries)
            ..where((tbl) => tbl.id.equals(entryId)))
          .go();
    });
  }
}