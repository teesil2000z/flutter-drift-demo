// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'all_categories.dart';

// ignore_for_file: type=lint
mixin _$AllCategoriesMixin on DatabaseAccessor<MyDatabase> {
  $CategoriesTable get categories => attachedDatabase.categories;
  $TodoEntriesTable get todoEntries => attachedDatabase.todoEntries;
}
