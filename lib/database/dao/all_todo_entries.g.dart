// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'all_todo_entries.dart';

// ignore_for_file: type=lint
mixin _$AllTodoEntriesMixin on DatabaseAccessor<MyDatabase> {
  $CategoriesTable get categories => attachedDatabase.categories;
  $TodoEntriesTable get todoEntries => attachedDatabase.todoEntries;
}
