import 'package:drift/drift.dart';
import 'package:flutter_drift_demo/database/dao/all_categories.dart';
import 'package:flutter_drift_demo/database/dao/all_todo_entries.dart';
import 'package:flutter_drift_demo/database/definition/category.dart';
import 'package:flutter_drift_demo/database/definition/todo_entry.dart';
import 'definition/helpers/color_converter.dart';
import 'dart:ui' show Color;

part 'database.g.dart';

@DriftDatabase(
  tables: [TodoEntries, Categories], 
  daos: [
    AllTodoEntries,
    AllCategories
  ],
)
class MyDatabase extends _$MyDatabase {
  MyDatabase(QueryExecutor executor) : super(executor);

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(beforeOpen: (details) async {
        await customStatement('PRAGMA foreign_keys = ON');
      }, onCreate: (m) async {
        await m.createAll();
      }
    );
  }
}
