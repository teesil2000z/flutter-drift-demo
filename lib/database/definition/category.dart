import 'package:drift/drift.dart';
import 'package:flutter_drift_demo/database/definition/helpers/auto_increment_mixin.dart';
import 'package:flutter_drift_demo/database/definition/helpers/color_converter.dart';

@DataClassName('Category')
class Categories extends Table with AutoIncrementingPrimaryKey {
  TextColumn get name => text()();
  IntColumn get color => integer().map(const ColorConverter())();
}
