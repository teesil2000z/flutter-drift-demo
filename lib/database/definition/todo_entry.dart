import 'package:drift/drift.dart';
import 'package:flutter_drift_demo/database/definition/category.dart';
import 'package:flutter_drift_demo/database/definition/helpers/auto_increment_mixin.dart';

@DataClassName('TodoEntry')
class TodoEntries extends Table with AutoIncrementingPrimaryKey {
  TextColumn get description => text()();
  IntColumn get category => integer().nullable().references(Categories, #id)();
  DateTimeColumn get dueDate => dateTime().nullable()();
}




