import 'dart:io';

import 'package:archive/archive_io.dart';
import 'package:file_picker/file_picker.dart';
import 'package:file_picker_writable/file_picker_writable.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';

class DbAndFilesTransfer {

  Future<void> exportToDir() async {
    Directory dir = await getTemporaryDirectory();
    String filename = 'dbname-data-dumb-${_getTimestamp()}.zip';
    String pathToZip = p.join(dir.path, filename);
    File dbFile = await _getDbFile();
    String appPath = p.join((await getApplicationDocumentsDirectory()).path, 'projects');
    Directory appDir = Directory(appPath);

    var encoder = ZipFileEncoder();
    encoder.create(pathToZip);

    if (appDir.existsSync()) {
      encoder.addDirectory(appDir);
    }

    encoder.addFile(dbFile);
    encoder.close();

    File zip = File(pathToZip);

    final fileInfo = await FilePickerWritable().openFileForCreate(
      fileName: filename,
      writer: (file) async {
        zip.copySync(file.path);
      },
    );
    if (fileInfo == null) {
      return;
    }

    zip.deleteSync();

    return;
  }

  Future<void> importToDir() async {
    final result = await FilePicker.platform.pickFiles(allowMultiple: false);
    if(result != null){
      var decoder = ZipDecoder();
      final inputStream = InputFileStream(result.files[0].path!);
      final archive = decoder.decodeBuffer(inputStream);
      for (var file in archive.files) { 
        if (file.isFile) {
          if(file.name.contains('dbname.sqlite')){
            _importDBFile(file);
          }
        }
      }
    }
  }

  String _getTimestamp() {
    DateTime dateTime = DateTime.now();
    return '${dateTime.year}-${dateTime.month}-${dateTime.day}_${dateTime.hour}-${dateTime.minute}';
  }

  Future<bool> _importDBFile(ArchiveFile importedFile) async{
    File file = await _getDbFile();
    try {
      return file.writeAsBytes(await importedFile.content, flush: true).then((value) => true)
        .onError((error, stackTrace) {
          return Future.error("Error $error");
        });
    } on Exception catch (error) {
      return Future.error("Error $error");
    }
  }

  static Future<File> _getDbFile() async {
    var dir = await getApplicationDocumentsDirectory();
    final String path = '${dir.path}/dbname.sqlite';
    final File file = File(path);
    return file;
  }
}
