
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_drift_demo/bloc/home/home_cubit.dart';
import 'package:flutter_drift_demo/bloc/home/home_state.dart';
import 'package:flutter_drift_demo/database/database.dart';
import 'package:flutter_drift_demo/database/db_file_transfer.dart';
import 'package:flutter_drift_demo/di.dart';
import 'package:flutter_drift_demo/model/todo_entry_with_category.dart';
import 'package:flutter_drift_demo/presentation/home/widgets/card.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Provider<HomeCubit>(
      create: (context) => HomeCubit(HomeState(
        entries: di<MyDatabase>().allTodoEntries.entriesInCategory(null), 
        todoDescription: ''
      )),
      child: BlocBuilder<HomeCubit, HomeState>(
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: const Text('Drift Todo list'),
              actions: !kIsWeb ? [
                IconButton(
                  onPressed: (){
                    DbAndFilesTransfer().importToDir();
                  }, 
                  icon: const Icon(Icons.download)
                ),
                IconButton(
                  onPressed: (){
                    DbAndFilesTransfer().exportToDir();
                  }, 
                  icon: const Icon(Icons.upload)
                ),
              ] : [],
            ),
            body: StreamBuilder<List<TodoEntryWithCategory>>(
              stream: state.entries,
              builder: (context, snapshot) {
                return snapshot.hasData ? ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    return TodoCard(snapshot.data![index].entry);
                  },
                ) : const Center(child: CircularProgressIndicator());
              },
            ),
            bottomSheet: Material(
              elevation: 12,
              child: SafeArea(
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const Text('What needs to be done?'),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: TextFormField(
                              initialValue: state.todoDescription,
                              onChanged: (value) => context.read<HomeCubit>().updateTodoDescription(value),
                            ),
                          ),
                          IconButton(
                            icon: const Icon(Icons.send),
                            color: Theme.of(context).colorScheme.secondary,
                            onPressed: () => context.read<HomeCubit>().save(di<MyDatabase>()),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        }
      ),
    );
  }
}