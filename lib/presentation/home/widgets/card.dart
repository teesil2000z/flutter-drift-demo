import 'package:flutter/material.dart';
import 'package:flutter_drift_demo/database/database.dart';
import 'package:flutter_drift_demo/di.dart';
import 'todo_edit_dialog.dart';

/// Card that displays an entry and an icon button to delete that entry
class TodoCard extends StatelessWidget {
  final TodoEntry entry;

  const TodoCard(this.entry, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(entry.description),
                  if (entry.dueDate != null)
                    Text(
                      entry.dueDate.toString(),
                      style: const TextStyle(fontSize: 12),
                    )
                  else
                    const Text(
                      'No due date set',
                      style: TextStyle(color: Colors.grey, fontSize: 12),
                    ),
                ],
              ),
            ),
            IconButton(
              icon: const Icon(Icons.edit),
              color: Colors.blue,
              onPressed: () {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (ctx) => TodoEditDialog(entry: entry),
                );
              },
            ),
            IconButton(
              icon: const Icon(Icons.delete),
              color: Colors.red,
              onPressed: () {
                di<MyDatabase>().allTodoEntries.deleteTodoEntry(entry.id);
              },
            )
          ],
        ),
      ),
    );
  }
}
