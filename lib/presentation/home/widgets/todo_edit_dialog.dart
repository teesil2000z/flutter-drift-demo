import 'package:flutter/material.dart';
import 'package:flutter_drift_demo/bloc/home/todo_edit_dialog/todo_edit_dialog_bloc.dart';
import 'package:flutter_drift_demo/database/database.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_drift_demo/di.dart';
import 'package:provider/provider.dart';


class TodoEditDialog extends StatelessWidget {
  final TodoEntry entry;

  const TodoEditDialog({Key? key, required this.entry}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = TodoEditDialogCubit(TodoEditDialogState(description: '', entry: entry, selectedDate: entry.dueDate ?? DateTime.now()));
    return  Provider<TodoEditDialogCubit>(
      create: (context) => bloc,
      child: AlertDialog(
        title: const Text('Edit entry'),
        content: BlocBuilder<TodoEditDialogCubit, TodoEditDialogState>(
          builder: (context, state) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                TextFormField(
                  initialValue: state.description,
                  decoration: const InputDecoration(
                    hintText: 'What needs to be done?',
                    helperText: 'Content of entry',
                  ),
                  onChanged: (value) => context.read<TodoEditDialogCubit>().updateDescription(value),
                ),
                Row(
                  children: <Widget>[
                    IconButton(
                      icon: const Icon(Icons.calendar_today),
                      onPressed: () async {
                        final now = DateTime.now();
                        final initialDate = state.entry.dueDate ?? now;
                        final firstDate =
                            initialDate.isBefore(now) ? initialDate : now;
        
                        final selectedDate = await showDatePicker(
                          context: context,
                          initialDate: initialDate,
                          firstDate: firstDate,
                          lastDate: DateTime(3000),
                        );
                        if (selectedDate != null) context.read<TodoEditDialogCubit>().updateSelectedDate(selectedDate);
                      },
                    ),
                  ],
                ),
              ],
            );
          }
        ),
        actions: [
          TextButton(
            style: ButtonStyle(
              textStyle: MaterialStateProperty.all(
                const TextStyle(color: Colors.black),
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Cancel'),
          ),
          TextButton(
            child: const Text('Save'),
            onPressed: () {
              bloc.save(di<MyDatabase>());
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}
