import 'package:flutter/material.dart';
import 'package:flutter_drift_demo/di.dart';
import 'package:flutter_drift_demo/router.dart';

void main() async {
  await DependencyInjectionManager.initDependencies();
  runApp(MaterialApp.router(
    title: 'Flutter drift demo',
    routerConfig: AppRouter.router,
  ));
}
