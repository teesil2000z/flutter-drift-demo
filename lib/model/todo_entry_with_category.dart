import 'package:flutter_drift_demo/database/database.dart';

class TodoEntryWithCategory {
  final TodoEntry entry;
  final Category? category;

  TodoEntryWithCategory({required this.entry, this.category});
}