
import 'package:flutter_drift_demo/presentation/home/home_page.dart';
import 'package:go_router/go_router.dart';

class AppRouter{
  static final router = GoRouter(
    routes: [
      GoRoute(
        path: AppRoutes.home,
        builder: (_, __) => const HomePage(),
      ),
    ]
  );
}

class AppRoutes{
  static const String home = '/';
}